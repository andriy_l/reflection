package com.brainacad.oop.testnest1;

class Student{
    private String name;
    private double excellence;

    public Student() {
    }

    public Student(String name, double excellence) {
        this.name = name;
        this.excellence = excellence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        assert (!name.equals("Anonymous"));
    }

    public double getExcellence() {
        return excellence;
    }

    public void setExcellence(double excellence) {
        this.excellence = excellence;
    }

}
