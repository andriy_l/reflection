package com.brainacad.oop.testnest1;

import java.io.File;
import java.lang.reflect.*;

public class Main {

    public static void main(String[] args) {
        Student studentAndriy = new Student("Andriy", 10);

        Class classStudentAndriy = studentAndriy.getClass();

        System.out.println("We analyze:" + classStudentAndriy);

        Constructor[] studentAndriyConstructors = classStudentAndriy.getDeclaredConstructors();
        Method[] studentAndriyMethods = classStudentAndriy.getDeclaredMethods();
        Field[] studentAndriyFields = classStudentAndriy.getDeclaredFields();

        for(Constructor c : studentAndriyConstructors){
            String cModNames = Modifier.toString(c.getModifiers());
            Parameter[] cParams = c.getParameters();
            System.out.print("Constructor " + cModNames + " " + c.getName()+ " has parameters: ");
            for (Parameter p : cParams) {
                System.out.print(p.getType().getTypeName()+" "+p.getName()+", ");
            }

            System.out.println();
        }



        Field classStudentAndriyNameField = null;
        Field classStudentAndriyExcellenceField = null;

        Object fieldOfclassStudentAndriyNameVAlue = null;
        Object fieldOfclassStudentAndriyExcellenceVAlueAsObject = null;
        double fieldOfclassStudentAndriyExcellenceVAlueAsDouble = 0;
        try {

            classStudentAndriyNameField = classStudentAndriy.getDeclaredField("name");
            classStudentAndriyExcellenceField = classStudentAndriy.getDeclaredField("excellence");

            // allow to access to private fields
            // before (!!!) read it

            classStudentAndriyNameField.setAccessible(true);
            classStudentAndriyExcellenceField.setAccessible(true);

            fieldOfclassStudentAndriyNameVAlue = classStudentAndriyNameField.get(studentAndriy);
            fieldOfclassStudentAndriyExcellenceVAlueAsObject = classStudentAndriyExcellenceField.get(studentAndriy);
            fieldOfclassStudentAndriyExcellenceVAlueAsDouble = classStudentAndriyExcellenceField.getDouble(studentAndriy);
            // read from real class not Class-type

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("Name of " + studentAndriy.getClass().getName() + " " + fieldOfclassStudentAndriyNameVAlue.toString());

        // automatically wrapped
        System.out.println("Excellence of (Double) " + studentAndriy.getClass().getSimpleName() + " " + fieldOfclassStudentAndriyExcellenceVAlueAsObject.toString());
        // or Field.getDouble
        System.out.println("Excellence of (double) " + studentAndriy.getClass().getName() + " " + fieldOfclassStudentAndriyExcellenceVAlueAsDouble);

        // set value of fields
        try {
            classStudentAndriyExcellenceField.setDouble(studentAndriy, fieldOfclassStudentAndriyExcellenceVAlueAsDouble*2);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("\nStudent new value " + studentAndriy.getExcellence());


    }
}
