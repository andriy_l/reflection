package com.brainacad.oop.testnest1;


import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

public class PrimitiveDemo {
    public static void main(String[] args) {
        Student andriyStudent = new Student("Andriy", 100);

        Class andriyStudentClass = andriyStudent.getClass();
        Field[] andriyStudentClassFields = andriyStudentClass.getDeclaredFields();

        for(Field f : andriyStudentClassFields){
            Type t = f.getType();


            System.out.println(Modifier.toString(f.getModifiers()) + " " +t.getTypeName() + " " + f.getName());

         // double is actually not a class,
         // but double.class is the object used in reflection to indicate that an argument
         // or return type has primitive type double
            String type = t.getTypeName();
            Class cl;
            try {
                cl = Class.forName(type);
                System.out.println(type + " successfully created");
            } catch (ClassNotFoundException e) {
                System.out.println(type + " sorry it is primitive");
            }
            // or without exception
            if(f.getType().isPrimitive()){
                System.out.println(type + " sorry it is primitive");
            }

        }

        try {
            Field excellenceField = andriyStudentClass.getDeclaredField("excellence");
            excellenceField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

    }
}
