package com.brainacad.oop.testrecflection1;

import java.lang.reflect.Field;

/**
 * Created by andriy on 3/22/17.
 */
public class Main52 {
    public static void main(String[] args) {
        String myStr = "abcd";
        Class myStrClass = myStr.getClass();
        Field charValArray = null;
        try {
            charValArray = myStrClass.getDeclaredField("value");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        charValArray.setAccessible(true);

        char[] array = null;
        try {
            array = (char[]) charValArray.get(myStr);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("Old string value:" + myStr);
        char[] newArray = {'z', 'x', 'c', 'v'};
        try {
            charValArray.set(myStr, newArray);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        System.out.println("New string value:" + myStr);
        System.out.println("abcd");
    }
}
