package com.brainacad.oop.testrecflection1;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by andriy on 3/22/17.
 */
public class Main53 {
    public static void main(String[] args) {

        Class myC = null;
        try {
            myC = Class.forName("com.brainacad.oop.testrecflection1.MyClass");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        MyClass myClass = null;

        try {
            Constructor c = myC.getDeclaredConstructor(int.class);
             myClass = (MyClass) c.newInstance(22);

        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        System.out.println("We just create " + myClass);
        Method m = null;
        try {
            m = myC.getDeclaredMethod("setA", int.class);
                m.invoke(myClass, 33);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException  e) {
            e.printStackTrace();
        }

        System.out.println("after dynamically invoking method " + myClass);




    }
}
