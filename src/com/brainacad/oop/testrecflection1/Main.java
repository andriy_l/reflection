package com.brainacad.oop.testrecflection1;

import java.lang.reflect.*;

/**
 * Created by andriy on 3/22/17.
 */
public class Main {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        Class c = myClass.getClass();
        System.out.println("Modifiers: \n" + Modifier.toString(c.getModifiers())+"\n");

        System.out.println("Public fields: ");
        Field[] publicFields = c.getFields();
        for (Field f : publicFields){
            System.out.println("Name: " + f.getName());
            System.out.println("Type: " + f.getType());
        }
        System.out.println("\nAll fields:");
        Field[] allFields = c.getDeclaredFields();
        for (Field f : allFields){
            System.out.println("Name: " + f.getName());
            System.out.println("Type: " + f.getType());
        }
        System.out.println("\nConstructors:");
        Constructor[] constructors = c.getDeclaredConstructors();
        int i = 0;
        for (Constructor constructor : constructors){
            System.out.println("Constructor: " + (i++));
            Parameter[] cParams = constructor.getParameters();
            for (Parameter p : cParams) {
                System.out.print(p.getType() +" ");
            }
        }
        System.out.println("\nMethods:");
        Method[] methods = c.getDeclaredMethods();
        for (Method m : methods){
            System.out.println("Name: " + m.getName());
            System.out.println("Return type: "+m.getReturnType());
            Parameter[] cParams = m.getParameters();
            for (Parameter p : cParams) {
                System.out.println("Param types: "+p.getType());
            }
        }




    }
}
