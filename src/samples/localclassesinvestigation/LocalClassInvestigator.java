package samples.localclassesinvestigation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Vector;

public class LocalClassInvestigator {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Class<VictimClass4ReflectionTesting> victimClass4ReflectionTestingClass = VictimClass4ReflectionTesting.class;


        // 1 way

//        Class cl = null;
//        try {
//            cl = Class.forName("samples.localclassesinvestigation.VictimClass4ReflectionTesting$1LocalClass");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        Constructor[] constructors = cl.getDeclaredConstructors();
//        System.out.println(constructors[0].getName());
//        constructors[0].setAccessible(true);
//        System.out.println(constructors[0].getName());

        // 2 way

        ClassLoader classLoader = VictimClass4ReflectionTesting.class.getClassLoader();
        Field classes = ClassLoader.class.getDeclaredField("classes");
        classes.setAccessible(true);
        Vector<Class<?>> loadedClasses = (Vector<Class<?>>)classes.get(classLoader);
        for (Class<?> c : loadedClasses) {
            if(loadedClasses.toString().contains("$")) {
                System.out.println("classes: " + loadedClasses);
            }
        }




    }
}
