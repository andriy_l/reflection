package samples.singletoncracker;

public class SingletonVictim {
    private static SingletonVictim ourInstance = new SingletonVictim();

    public static SingletonVictim getInstance() {
        return ourInstance;
    }

    private SingletonVictim() {
        System.out.println("New instance created");
    }
}
