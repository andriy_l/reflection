package samples.singletoncracker;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class SingletonCracker {
    public static void main(String[] args) {
        Class<SingletonVictim> singletonVictimClass = SingletonVictim.class;
        Constructor[] constructors = singletonVictimClass.getDeclaredConstructors();
        constructors[0].setAccessible(true);
        SingletonVictim singletonVictim1 = null;
        SingletonVictim singletonVictim2 = null;
        try {
            singletonVictim1 = (SingletonVictim) constructors[0].newInstance();
            singletonVictim2 = (SingletonVictim) constructors[0].newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println(singletonVictim1.hashCode());
        System.out.println(singletonVictim2.hashCode());
        System.out.println(singletonVictim1.equals(singletonVictim2));
    }
}
