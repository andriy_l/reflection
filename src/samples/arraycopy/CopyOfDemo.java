package samples.arraycopy;

import java.lang.reflect.*;
import java.util.*;

/**
 * This program demonstrates the use of reflection for manipulating arrays.
 * @version 1.2 2012-05-04
 * @author Cay Horstmann
 */
public class CopyOfDemo
{
    public static void main(String[] args)
    {
        int[] a = { 1, 2, 3 };
        a = (int[]) goodCopyOf(a, 10);
        System.out.println(Arrays.toString(a));

        String[] b = { "Tom", "Dick", "Harry" };
        b = (String[]) goodCopyOf(b, 10);
        System.out.println(Arrays.toString(b));

        System.out.println("The following call will generate an exception.");
        b = (String[]) badCopyOf(b, 10);
    }

    /**
     * бед копі, бо  потім масив об'єктів неможливо скастити
     * і не можливо використати для масивів примітивних типів даних
     * This method attempts to grow an array by allocating a new array and copying all elements.
     * @param a the array to grow
     * @param newLength the new length
     * @return a larger array that contains all elements of a. However, the returned array has
     * type Object[], not the same type as a
     */
    public static Object[] badCopyOf(Object[] a, int newLength) // not useful
    {
        Object[] newArray = new Object[newLength];
        System.arraycopy(a, 0, newArray, 0, Math.min(a.length, newLength));
        return newArray;
    }

    /**
     * This method grows an array by allocating a new array of the same type and
     * copying all elements.
     * @param a the array to grow. This can be an object array or a primitive
     * type array
     * @return a larger array that contains all elements of a.
     */
    public static Object goodCopyOf(Object a, int newLength)
    {
        // 1 визначаємо якому класу належить об'єкт типу a
        Class cl = a.getClass();
        // чи це дійсно масив?
        if (!cl.isArray()) return null;

        // отримати тип масиву. getComponentType() визначений лише для типу об'єктів типу масив
        Class componentType = cl.getComponentType();
        // статичний метод, який повертає довжину будь-якого масиву
        int length = Array.getLength(a);
        // створюємо новий масив типу componentType, довжиною newLength
        Object newArray = Array.newInstance(componentType, newLength);
        // користуємось системним методом arraycopy, щоб скопіювати
        // a - звідки (source Array)
        // 0 - стартова позиція source Array
        // newArray - куди (destination array)
        // 0 - стартова позиція в destination array
        // Math.min(length, newLength) - кількість елементів, які треба скопіювати
        // Бо... якщо вказаний новий масив буде меншим за існуючий
        System.arraycopy(a, 0, newArray, 0, Math.min(length, newLength));
        return newArray;
    }
}
