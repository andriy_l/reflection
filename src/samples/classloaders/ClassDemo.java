package samples.classloaders;

import sun.java2d.loops.ProcessPath;

import java.lang.*;


/**
 * The java.lang.Class.getClassLoader() method returns the class loader for the class.
 * Some implementations may use null to represent the bootstrap class loader.
 * The method will return null in such implementations if this class was loaded by the bootstrap class loader.
 Declaration

 Following is the declaration for java.lang.Class.getClassLoader() method

 public ClassLoader getClassLoader()

 Parameters

 NA
 Return Value

 This method returns the class loader that loaded the class or interface represented by this object.
 Exception

 SecurityException − If a security manager exists and its checkPermission method denies access to the class loader for the class.
 *
 */
public class ClassDemo {

    public static void main(String[] args) {

        // In Java 8
        System.out.println(ClassLoader.getSystemResource("java/lang/Class.class"));
        // result will be:
        // jar:file:/usr/local/jdk8/jre/lib/rt.jar!/java/lang/Class.class

        System.out.println();
        try {
            // returns the Class object associated with this class
            Class cls = Class.forName("samples.classloaders.ClassDemo");

            // returns the ClassLoader object associated with this Class.
            ClassLoader cLoader = cls.getClassLoader();

            if (cLoader == null) {
                System.out.println("The default system class was used.");
            } else {
                // returns the class loader
                Class loaderClass = cLoader.getClass();

                System.out.println("Class associated with ClassLoader = " +
                        loaderClass.getName());
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.toString());
        }
    }
}