package samples.classloaders.custom;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by andriy on 20.03.17.
 */
public class ClassLoaderDemo {

    public static void main(String[] args) {

        JavaClassLoader javaClassLoader = new JavaClassLoader();
        javaClassLoader
                .invokeClassMethod("samples.classloaders.custom.MyClass",
                        "sayHello");
        ClassLoader cl2 = javaClassLoader.getParent();
        System.out.println(cl2);
        ClassLoader cl3 = cl2.getParent();
        System.out.println(cl3);
        ClassLoader cl4 = cl3.getParent();
        System.out.println(cl4);
        URL url2class = null;
        try {
            url2class = new URL("file:///home/andriy/Documents/brainacad-java/p2/2_1_10_junit/junit-4.12.jar");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        URLClassLoader urlClassLoader
                = new URLClassLoader(new URL[]{url2class});
        try {
            Class drv = urlClassLoader.loadClass("org.junit.Test");
            System.out.println(drv.getCanonicalName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

}